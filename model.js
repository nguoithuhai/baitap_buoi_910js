// lớp đối tượng
function nhanvien(
  taikhoannhap,
  hotennhap,
  emailnhap,
  matkhaunhap,
  ngaylamnhap,
  luongcobannhap,
  chucvunhap,
  giolamnhap
) {
  this.taikhoan = taikhoannhap;
  this.hovaten = hotennhap;
  this.email = emailnhap;
  this.matkhau = matkhaunhap;
  this.ngaylam = ngaylamnhap;
  this.luongcoban = luongcobannhap;
  this.chucvu = chucvunhap;
  this.giolam = giolamnhap;
  this.tongluong = function () {
    if (this.chucvu == "Sếp") {
      return this.luongcoban * 1 * 3;
    } else if (this.chucvu == "Trưởng phòng") {
      return this.luongcoban * 1 * 2;
    } else {
      return this.luongcoban * 1;
    }
  };
  this.xeploai = function () {
    if (this.giolam * 1 >= 192) {
      return "xuất sắc";
    } else if (this.giolam * 1 < 192 && this.giolam * 1 >= 176) {
      return "giỏi";
    } else if (this.giolam * 1 < 176 && this.giolam * 1 >= 160) {
      return "khá";
    } else {
      return "trung bình";
    }
  };
}
