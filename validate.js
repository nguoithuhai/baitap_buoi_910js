// hàm show thông báo
function showmessage(idtag, message) {
  document.getElementById(idtag).style.display = "block";
  document.getElementById(idtag).innerHTML = message;
}
// hơp lệ ==>true
// kiểm tra mã sinh viên có trùng hay không
function kiemtratrung(id, dsnv) {
  var vitri = dsnv.findIndex(function (sv) {
    return sv.taikhoan == id;
  });
  if (vitri != -1) {
    // tim thấy
    showmessage("tbTKNV", "tài khoản nhân viên đã tồn tại");
    return false;
  } else {
    showmessage("tbTKNV", " ");
    return true;
  }
}
//kiểm tra email
function kiemtraemail(email) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  var ismemail = re.test(email);
  if (ismemail) {
    showmessage("tbEmail", " ");
    return true;
  } else {
    showmessage("tbEmail", "email không hợp lệ ");
    return false;
  }
}
// kiểm tra độ dài
function kiemtradodai(value, iderr, min, max) {
  var length = value.length;
  if (length >= min && length <= max) {
    showmessage(iderr, " ");
    return true;
  } else {
    showmessage(iderr, `trường này phải gồm ${min} đến ${max} kí tự `);
    return false;
  }
}
// kiểm tra tên user
function kiemtrachuoi(value, iderr) {
  var re = /^[a-zA-Z\-]+$/;
  if (re.test(value)) {
    showmessage(iderr, "");
    return true;
  } else {
    showmessage(iderr, "Trường này chỉ gồm chuỗi");
    return false;
  }
}
function kiemtramatkhau(value, iderr) {
  var mk = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,10}$/;
  if (mk.test(value)) {
    showmessage(iderr, "");
    return true;
  } else {
    showmessage(
      iderr,
      "mật Khẩu từ 6-10 ký tự, ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt"
    );
    return false;
  }
}
function kiemtrangaylam(value, iderr) {
  // định dạng DD MM YYYY
  //   /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/
  // định dạng DD MM YYYY

  // định dạng mm dd yyyy
  // (0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])\/((19|20)\d\d)
  var ngaylamkiemtra =
    /^((((0[13578]|1[02])[/](0[1-9]|1[0-9]|2[0-9]|3[01]))|((0[469]|11)[/](0[1-9]|1[0-9]|2[0-9]|3[0]))|((02)([/](0[1-9]|1[0-9]|2[0-8]))))[/](19([6-9][0-9])|20([0-9][0-9])))|((02)[/](29)[/](19(6[048]|7[26]|8[048]|9[26])|20(0[048]|1[26]|2[048])))/;
  // mm dd yyyy

  if (ngaylamkiemtra.test(value)) {
    showmessage(iderr, "");
    return true;
  } else {
    showmessage(iderr, "ngày làm không đúng định dạng MM DD YYYY");
    return false;
  }
}
function kiemtraluong(value, iderr) {
  var min = 1000000;
  var max = 20000000;
  if (value >= min * 1 && value <= max * 1) {
    showmessage(iderr, "");
    return true;
  } else {
    showmessage(iderr, "trường này từ 1.000.000 đến 20.000.000");
    return false;
  }
}
function kiemtrachucvu(value, iderr) {
  if (value == "Sếp" || value == "Trưởng phòng" || value == "Nhân viên") {
    showmessage(iderr, "");
    return true;
  } else {
    showmessage(iderr, "trường này không để trống");
    return false;
  }
}
function kiemtragiolam(value, iderr) {
  var min = 80;
  var max = 200;
  if (value >= min && value <= max) {
    showmessage(iderr, "");
    return true;
  } else {
    showmessage(iderr, "trường này từ 80 đến 200");
    return false;
  }
}
