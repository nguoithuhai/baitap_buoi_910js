//  array :splice, findIndex,map,push
// lấy thông tin từ form
// thêm sv
var dsnv = [];
const dsnv_LOCAL = "dsnv_LOCAL";

// khi user load trang => lấy dữ liệu từ localStorage

var jsonData = localStorage.getItem(dsnv_LOCAL);
if (jsonData != null) {
  dsnv = JSON.parse(jsonData).map(function (item) {
    return new nhanvien(
      item.taikhoan,
      item.hovaten,
      item.email,
      item.matkhau,
      item.ngaylam,
      item.luongcoban,
      item.chucvu,
      item.giolam
    );
  });
  renderdsnv(dsnv);
}

function themnguoidung() {
  var sv = laythongtintuform();
  // viết new Sinh Vien để co method tongluong và
  // xeploai;
  //validate
  // kiểm tra trùng tài khoản và độ dài
  var isvalid =
    kiemtratrung(sv.taikhoan, dsnv) &&
    kiemtradodai(sv.taikhoan, "tbTKNV", 4, 6);
  //end kiểm tra trùng tài khoản và độ dài
  // kiểm tra user là ký tự
  isvalid = isvalid & isvalid & kiemtrachuoi(sv.hovaten, "tbTen");
  // end kiểm tra user là ký tự
  // kiểm tra email
  isvalid = isvalid & kiemtraemail(sv.email);
  // end kiểm tra email
  // kiểm tra mat khau
  isvalid = isvalid & kiemtramatkhau(sv.matkhau, "tbMatKhau");
  isvalid = isvalid & kiemtrangaylam(sv.ngaylam, "tbNgay");
  isvalid = isvalid & kiemtraluong(sv.luongcoban, "tbLuongCB");
  isvalid = isvalid & kiemtrachucvu(sv.chucvu, "tbChucVu");
  isvalid = isvalid & kiemtragiolam(sv.giolam, "tbGiolam");
  // end kiểm tra mat khau
  if (isvalid) {
    var newNhanVien = new nhanvien(
      sv.taikhoan,
      sv.hovaten,
      sv.email,
      sv.matkhau,
      sv.ngaylam,
      sv.luongcoban,
      sv.chucvu,
      sv.giolam
    );
    //   push thêm vào đuôi của mảng
    dsnv.push(newNhanVien);
    console.log("thêm nv mới: ", newNhanVien);
    // convert data --> lưu vào localstorage
    let dataJsong = JSON.stringify(dsnv);
    // console.log("dsnv: ", dsnv);
    // lưu vào localStorage
    localStorage.setItem(dsnv_LOCAL, dataJsong);
    //   render dsnv lên table

    renderdsnv(dsnv); // tạo render giao diện
  }
}

function xoasv(id) {
  //splice

  console.log("xoasv: id ", id);

  //   splice là cut
  // slice là copy
  //tim vị trí
  var vitri = -1;
  for (var i = 0; i < dsnv.length; i++) {
    if (dsnv[i].taikhoan == id) {
      vitri = i;
    }
  }

  if (vitri != -1) {
    // console.log("truoc xoa sv:-->dsnv ", dsnv.length, vitri);
    dsnv.splice(vitri, 1);
    let dataJsong = JSON.stringify(dsnv);
    // console.log("dsnv: ", dsnv);
    // lưu vào localStorage
    localStorage.setItem(dsnv_LOCAL, dataJsong);
    renderdsnv(dsnv); //--> tạo lại giao diện
  }
  //   console.log("vitri: ", vitri);
}
function suasv(id) {
  console.log("id: ", id);
  var vitri = dsnv.findIndex(function (item) {
    return item.taikhoan == id;
  });
  console.log("vitri: ", vitri);
  // show thông tin lên form
  var sv = dsnv[vitri];
  document.getElementById("tknv").value = sv.taikhoan;
  document.getElementById("name").value = sv.hovaten;
  document.getElementById("email").value = sv.email;
  document.getElementById("password").value = sv.matkhau;
  document.getElementById("datepicker").value = sv.ngaylam;
  document.getElementById("luongCB").value = sv.luongcoban;
  document.getElementById("chucvu").value = sv.chucvu;
  document.getElementById("gioLam").value = sv.giolam;
}
function capnhat() {
  //lấy thong tin tu form
  var sv = laythongtintuform();
  console.log("capnhatsv: ", sv);
  // document.getElementById()
  var isvalid = kiemtradodai(sv.taikhoan, "tbTKNV", 4, 6);
  //end kiểm tra trùng tài khoản và độ dài
  // kiểm tra user là ký tự
  isvalid = isvalid & isvalid & kiemtrachuoi(sv.hovaten, "tbTen");
  // end kiểm tra user là ký tự
  // kiểm tra email
  isvalid = isvalid & kiemtraemail(sv.email);
  // end kiểm tra email
  // kiểm tra mat khau
  isvalid = isvalid & kiemtramatkhau(sv.matkhau, "tbMatKhau");
  isvalid = isvalid & kiemtrangaylam(sv.ngaylam, "tbNgay");
  isvalid = isvalid & kiemtraluong(sv.luongcoban, "tbLuongCB");
  isvalid = isvalid & kiemtrachucvu(sv.chucvu, "tbChucVu");
  isvalid = isvalid & kiemtragiolam(sv.giolam, "tbGiolam");
  var newNhanVien = new nhanvien(
    sv.taikhoan,
    sv.hovaten,
    sv.email,
    sv.matkhau,
    sv.ngaylam,
    sv.luongcoban,
    sv.chucvu,
    sv.giolam
  );
  var vitri = dsnv.findIndex(function (item) {
    return item.taikhoan == sv.taikhoan;
  });
  dsnv[vitri] = newNhanVien;
  let dataJsong = JSON.stringify(dsnv);
  // console.log("dsnv: ", dsnv);
  // lưu vào localStorage
  localStorage.setItem(dsnv_LOCAL, dataJsong);
  renderdsnv(dsnv);
}

function searchsv() {
  var loainv = document.getElementById("searchName").value;
  console.log("loainv: ", loainv);
  var vitri = dsnv.findIndex(function (item) {
    return item.xeploai == loainv;
  });
  console.log("dsnv[vitri]: ", dsnv[vitri]);
  // show thông tin ra form
  // document.getElementById("btnSearch").value = dsnv[vitri];
}

// <!-- splice, findindex,map,push , for each -->
//   <!-- localstorage: dùng để lưu trữ data(dưới dạng string) , json : dùng để convert data -->

// diable input js
